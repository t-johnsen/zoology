﻿using System;
using System.Collections.Generic;
using System.Text;

/*
 * Animal class. Parent to Dog and Snake.
 * All children will have to eventually use one of the two constructures below.
 */
namespace zoology
{
    class Animal
    {

        public int Legs { get ; set; }
        public string Movement { get; set; }
        public string Food { get; set; }
        public string Name { get; set; }

        public Animal(int legs, string name, string movement, string food)
        {
            Legs = legs;
            Name = name;
            Movement = movement;
            Food = food;
        }

        public Animal(string name, string food)
        {
            Name = name;
            Food = food;
        }

        public void MakeSound(int seconds)
        {
            Console.WriteLine($"{Name} made a sound for {seconds} seconds.");
        }

        public virtual string PrintText()
        {
            return $"Name: {Name} - Legs: {Legs} - Food: {Food} - Movement: {Movement}";
        }

        public virtual void DoYourThing()
        {
            Console.WriteLine($"{Name} is acting out...");
        }
    }
}
