﻿using System;
using System.Collections.Generic;

namespace zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to Zoology! Below you can see the existing collection of animals:");

            List<Animal> animals = new List<Animal>
            {
                new Shiba(4, "Nick", "Fast as fudge", "Babies", "Full"),
                new GoldenRetriver(3, "Tucker", "Meehh", "Raw meat (not human)", "Looooong"),
                new Shiba(4, "Nick", "Meehh", "Babies", "Looooong"),
                new GoldenRetriver(4, "Dick", "Meehh", "Raw meat (not human)", "Looooong"),
                new Shiba(4, "Lick", "Meehh", "Raw meat (not human)", "Looooong"),
                new GoldenRetriver(4, "Hick", "Meehh", "Raw meat (not human)", "Looooong"),
                new Python(0, "Leon", "Meehh", "Raw meat (not human)", "Red"),
                new Python(0, "Neon", "Meehh", "Raw meat (not human)", "Green"),
                new Python(0, "Ceon", "Meehh", "Raw meat (not human)", "Red"),
                new Cobra("Nickeloo", "Green", "Babies"),
                new GoldenRetriver(4, "Hick", "Meehh", "Raw meat (not human)", "Looooong")
            };

            PrintAll(animals);
            bool done = false;

            // User interaction. Given choises for what to search for and the result is then printed to the user.
            while (!done)
            {
                Console.WriteLine("\nFilter list by name (n), by food (f), or by legs (l) (q for quit). Enter the one you prefer:");
                string choise = Console.ReadLine();
                if (choise == "n")
                {
                    Console.WriteLine("What name or part of name do you want to search for?");
                    string searchString = Console.ReadLine();
                    PrintAll((List<Animal>)FilteringAnimals.FilterByNmae(animals, searchString));
                }
                else if (choise == "l")
                {
                    Console.WriteLine("What number of legs do you want to search for?");
                    int xLegs = Int16.Parse(Console.ReadLine());
                    PrintAll((List<Animal>)FilteringAnimals.FilterByLegs(animals, xLegs));
                }
                else if (choise == "f")
                {
                    Console.WriteLine("What food do you want to search for?");
                    string foodString = Console.ReadLine();
                    PrintAll((List<Animal>)FilteringAnimals.FilterByFood(animals, foodString));
                }
                else if (choise == "q")
                {
                    done = true;
                }
                else
                {
                    Console.WriteLine("You entered a wrong carachter. Please try again.");
                }
            }

        }


        private static void PrintAll(List<Animal> animals)
        {
            foreach (Animal animal in animals)
            {
                Console.WriteLine(animal.PrintText());
            }
        }
    }
}
