﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace zoology
{
    /*
     * Static methods to search the animals list using LINQ.
     */
    class FilteringAnimals
    {
        // Filter list by name
        public static IEnumerable<Animal> FilterByNmae(List<Animal> animals, string filterString)
        {
            return
                (from animal in animals
                where animal.Name.ToLower().Contains(filterString.ToLower())
                select animal).ToList();
        }

        //Filter list by legs
        public static IEnumerable<Animal> FilterByLegs(List<Animal> animals, int xLegs)
        {
            return (from animal in animals 
                   where animal.Legs == xLegs 
                   select animal).ToList();
        }

        // Filter list by food
        public static IEnumerable<Animal> FilterByFood(List<Animal> animals, string foodString)
        {
            return (from animal in animals
                    where animal.Food.ToLower().Contains(foodString.ToLower())
                    select animal).ToList();
        }


    }
}
