﻿using System;
using System.Collections.Generic;
using System.Text;

/*
 * Abstract class Snake. Inherits from Animal and implements ISwim interface.
 */
namespace zoology
{
    abstract class Snake : Animal, ISwim
    {
        public string Color { get; set; }

        public Snake(string name, string color, string food) : base(name, food)
        {
            Color = color;
        }

        public Snake(int legs, string name, string movement, string food, string color) : base(legs, name, movement, food)
        {
            Color = color;
        }

        abstract public void Attack();

        public virtual string Swim(int minutes)
        {
            return $"{Name} the snake swam for {minutes} minutes.";
        }

        public void PeeInWater()
        {
            Console.WriteLine("I don't think snakes pee in the water(?).");
        }
    }
}
