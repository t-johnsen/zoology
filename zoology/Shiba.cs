﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zoology
{
    /*
     * Shiba inheriting from Dog
     */
    class Shiba : Dog
    {

        public Shiba(int legs, string name, string movement, string food, string fur) : base(legs, name, movement, food, fur)
        {

        }

        public override void Attack()
        {
            Console.WriteLine($"{Name} allways attack even though it's not supposed to...");
        }

        public override string Swim(int minutes)
        {
            return $"{Name} the {this.GetType().Name} don't wanna swim. Instead it slept for {minutes} minutes.";
        }
    }
}
