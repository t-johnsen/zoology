﻿using System;
using System.Collections.Generic;
using System.Text;

/*
 * Python class inherits from abstract parent Snake. Snake inherits from Animal.
 */
namespace zoology
{
    class Python : Snake
    {

        public Python(int legs, string name, string movement, string food, string color) : base (legs, name, movement, food, color)
        {
        }

        public override void Attack()
        {
            Console.WriteLine("Python is attacking! You will propably die... Sorry.");
        }

        public override void DoYourThing()
        {
            Console.WriteLine($"{Name} is dancing...");
        }

        public override string Swim(int minutes)
        {
            return $"{Name} the snake loves water. It stayed in there for {minutes} minutes.";
        }
    }
}
