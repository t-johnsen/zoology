﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

/*
 * Dog class inherits from Animal and implements IAttack and ISwim interfaces
 */
namespace zoology
{
    class Dog : Animal, IAttack, ISwim
    {
        public string Fur { get; set; }

        public Dog(int legs, string name, string movement, string food, string fur) : base(legs, name, movement, food)
        {
            Fur = fur;
        }

        public override string PrintText()
        {
            return base.PrintText() + $" - Fur: {Fur}";
        }

        public override void DoYourThing()
        {
            Console.WriteLine($"{Name} is barking");
        }

        public virtual void Attack()
        {
            Console.WriteLine($"{Name} does not attack because it's a {this.GetType().Name}");
        }

        public void Bite()
        {
            Console.WriteLine("Dogs don't bite!");
        }

        public void PeeInWater()
        {
            Console.WriteLine("The dog peed in the water...");
        }

        public virtual string Swim(int minutes)
        {
            return $"{Name} the {this.GetType().Name} swam for {minutes} minutes.";
        }

    }
}
