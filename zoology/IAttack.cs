﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zoology
{
    /*
     * Interface for attacking
     */
    interface IAttack
    {
        public void Attack();
        public void Bite();
    }
}
