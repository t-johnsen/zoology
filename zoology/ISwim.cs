﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zoology
{
    /*
     * Interface for swimmming
     */
    interface ISwim
    {
        public string Swim(int minutes);
        public void PeeInWater();
    }
}
