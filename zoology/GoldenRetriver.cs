﻿using System;
using System.Collections.Generic;
using System.Text;

namespace zoology
{
    /*
     * GoldenRetriver inheriting from Dog.
     */
    class GoldenRetriver : Dog
    {

        public GoldenRetriver(int legs, string name, string movement, string food, string fur) : base(legs, name, movement, food, fur)
        {

        }

        public override string Swim(int minutes)
        {
            return base.Swim(minutes) + " And it fetched the ball.";

        }
    }
}
