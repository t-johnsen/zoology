﻿using System;
using System.Collections.Generic;
using System.Text;

/*
 * Cobra class inherits from abstract parent Snake. Snake inherit from Animal.
 */
namespace zoology
{
    class Cobra : Snake
    {

        public Cobra(string name, string color, string food) : base(name, color, food)
        {
        }

        public override void Attack()
        {
            Console.WriteLine("Cobra is attacking!");
        }

    }
}
